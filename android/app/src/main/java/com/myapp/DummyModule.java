package com.myapp;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;
import android.util.Log;

public class DummyModule extends ReactContextBaseJavaModule {
    DummyModule(ReactApplicationContext context) {
        super(context);
    }
    @Override
    public String getName() {
        return "DummyModule";
    }
    @ReactMethod
    public void logValueOnNative(String name) {
        Log.d("Dummy Module Printing ",  name);
    }
}

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect } from 'react';
import {
  ActivityIndicator,
  StatusBar
} from 'react-native';
import 'react-native-gesture-handler';
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import './i18n';
import { resetAuthHeader } from './shared/api/configureAPI';
import { persistor, store } from './shared/redux/store';
import LocalizationProvider from './src/context/localizationProvider';
import Drawer from './src/navigators';
import { TYPOGRAPHY } from './src/view/styles/typography';

declare const global: {HermesInternal: null | {}};

const LocalizationContext = React.createContext({
  t: (key: any, data: any) => {},
  i18n: {},
});

const App = (props: any) => {
 
  const {t, i18n, children} = props;
  const user = store.getState().user;
  
  return (
    <>
      <LocalizationProvider>
        <Provider store={store}>
          <PersistGate  loading={<ActivityIndicator/>} persistor={persistor}>
            <MenuProvider
              customStyles={{
                backdrop: {backgroundColor: '#000', opacity: 0.5},
              }}>
              <NavigationContainer>
                <StatusBar backgroundColor={TYPOGRAPHY.COLOR.fourthColor} />
                <Drawer
                  screenProps={{
                    t,
                    i18n,
                  }}
                />
              </NavigationContainer>
            </MenuProvider>
          </PersistGate>
        </Provider>
      </LocalizationProvider>
    </>
  );
};

export default App;

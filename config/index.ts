//base for APIs
const base = '/api/v1';


/**
 * Domain for API
 */
const authDomain = 'https://accounts.spotify.com';
const domain = 'https://api.spotify.com'
const SPOTIFY_REDIRECT_DOMAIN = 'https://example.com';

/**
 * API Defaults Declaration
 */
const Defaults = {
  SPOTIFY_REDIRECT_DOMAIN,
  domain,
  authDomain,
  defaultLocale: {
    lang: 'en'
  },
  //All APIs can be listed here
  apis: {
    baseUrl: `${domain}`,
    authentication: {
      initialToken:`/api/token`
    },
    playlists: {
      myPlaylists: `/v1/browse/featured-playlists`
    }
  }
};

export default Defaults;

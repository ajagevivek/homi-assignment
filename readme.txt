# This project includes

react-native 63.4
react-navigation 5
react-redux with thunk middleware and redux persist
code is writted in typescript
internationalization is added 

Project structure explained :

├── App.tsx      [Application Starting point]
├── __tests__
│   └── App-test.tsx
├── app.json     [Application JSON]
├── babel.config.js
├── config       [All configuration stored in this file]
│   └── index.ts
├── declarations.d.ts  [react-native-svg typescript]
├── i18n.js      [internationalization translation file ]
├── index.js
├── metro.config.js 
├── package-lock.json
├── package.json
├── shared       [contains common functionality ]
│   ├── api      [API logic  for Application]
│   │   ├── configureAPI.ts
│   │   ├── constants.ts
│   │   └── index.ts
│   └── redux    [DATA store for Application]
│       ├── actions
│       ├── constants
│       ├── reducers
│       ├── store
│       └── thunk
├── src
│   ├── constants
│   │   └── screen.tsx
│   ├── context [Localization Context]
│   ├── navigators  [Navigator context]
│   └── view     [Application Views]
│       ├── assets
│       ├── elements
│       ├── screens
│       └── styles
├── tsconfig.json
└── yarn.lock
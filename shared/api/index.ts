
import { apiClient, authClient } from './configureAPI';
import config from '../../config';
import Constant from './constants';
import { AxiosResponse } from 'axios';
import qs from 'qs';



export interface APIReponse {
  data: any;
  status: number;
  error: any;
}



/**
 * APIs Defined Here
 */
export async function fetchPlayListing(): Promise<AxiosResponse> {
  return apiClient.get(config.apis.playlists.myPlaylists)
}


export async function fetchPlaylistTracks(playlistId:string): Promise<AxiosResponse> {
  return apiClient.get(`v1/playlists/${playlistId}/tracks`)
}



export function fetchAuthToken(code: string, refresh_token: string): Promise<AxiosResponse> {
  if (code) {
    return authClient.post(config.apis.authentication.initialToken, qs.stringify({
      grant_type: 'authorization_code',code, 
      redirect_uri: 'https://example.com/callback'
    }), {
      headers: {
        Authorization: 'Basic MzYxZDFkNmMxYWQ0NDdhN2E1OGZlNmZkYzE4OTQ2M2I6OTdjMzE5ZWI4N2YwNDI2Y2JmZDFhZmUxOWZkOWVjZTQ=',
        'Content-Type': 'application/x-www-form-urlencoded'

      }
    })
  } else {
    return authClient.post(config.apis.authentication.initialToken, qs.stringify({ refresh_token, grant_type: 'refresh_token' }), {
      headers: {
        Authorization: 'Basic MzYxZDFkNmMxYWQ0NDdhN2E1OGZlNmZkYzE4OTQ2M2I6OTdjMzE5ZWI4N2YwNDI2Y2JmZDFhZmUxOWZkOWVjZTQ=',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  }

}

import axios from 'axios'
import  Defaults from '../../config'

/**
 * API object export
 */
 export const apiClient = axios.create({
  baseURL: Defaults.apis.baseUrl,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
})

export const authClient = axios.create({
  baseURL: Defaults.authDomain,
  headers: {

  },
  timeout: 10000,
})


/**
 * Following Function can be used to set or reset authentication header on successful login
 */

export function setAuthHeader(token:string) {
  apiClient.defaults.headers['Authorization'] = `Bearer ${token}`
}
export function resetAuthHeader() {
  delete apiClient.defaults.headers['Authorization']
}



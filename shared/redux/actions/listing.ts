/**
 * Single location for Actions dispatched at `app` level
 */
import { ACTION_TYPES } from '../constants/actionTypes';


/**
 * Redux Actions for Listing Reducer
 */
export const setAvailibility = (data:any) => ({
  type: ACTION_TYPES.LISITNG.SET_LISTING,
  payload:data
});

export const setPlaylistTracks = (data:any) => ({
  type: ACTION_TYPES.LISITNG.SET_PLAYLIST_TRACKS,
  payload:data
});

export const resetPlaylistTracks = () => ({
  type: ACTION_TYPES.LISITNG.RESET_PLAYLIST_TRACKS,
});

export const sortAvailibility = (type:any) => ({
  type: ACTION_TYPES.LISITNG.SORT,
  payload:type
});

export const setCurrentPlaylist = (data:any) => ({
  type: ACTION_TYPES.LISITNG.SET_CURRENT_PLAYLIST,
  payload:data
});

export const resetCurrentPlaylist = () => ({
  type: ACTION_TYPES.LISITNG.CLEAR_CURRENT_PLAYLIST,
});

export const clearAvailibility = () => ({
    type: ACTION_TYPES.LISITNG.CLEAR_PLAYLISTS,
  });
  
import { ACTION_TYPES } from '../constants/actionTypes';

/**
 * Redux Actions for User Reducer
 */

export const login = (data:any) => ({
  type: ACTION_TYPES.USER.LOGIN,
  payload:data
});

export const logout = () => ({
    type: ACTION_TYPES.USER.LOGOUT,
  });
  
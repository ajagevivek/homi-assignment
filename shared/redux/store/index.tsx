import { applyMiddleware, createStore } from 'redux';
import * as thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistReducer, persistStore } from 'redux-persist'
import { setAuthHeader, resetAuthHeader } from '../../api/configureAPI';

import reducers from '../reducers';
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [
  ],
}

let middlewares = [thunkMiddleware.default];

if (__DEV__) {
  const logger = require('redux-logger');
  const loggerMiddleware = logger.createLogger({
    duration: true,
  });
  middlewares = [...middlewares, loggerMiddleware];
}

/**
 * Redux Persist for Persisting State across reload
 */
const persistedReducer = persistReducer(persistConfig, reducers)

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(...middlewares)));

const persistor = persistStore(store, {}, () => {
  const user = store.getState().user;
  console.log('persistor callbackl');
  console.log(user);
  if(user && user.user && user.user.access_token){
    setAuthHeader(user.user.access_token)
  }
})
export type AppDispatch = typeof store.dispatch;

export  {store,persistor};

import { ACTION_TYPES } from '../constants/actionTypes';

const initialState = {
  user: null,
};

/**
 * User Reducer for Login and Logout
 */ 

export default (state = initialState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.USER.LOGIN:
      return {
        user:action.payload,
      };
    case ACTION_TYPES.USER.LOGOUT:
        return {
          user:null,
        };
    default:
      return state;
  }
};

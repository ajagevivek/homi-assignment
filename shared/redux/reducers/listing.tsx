import {ACTION_TYPES} from '../constants/actionTypes';
import {
  SORT_TYPE,
  DEFAULT_SORT,
  ETA_SORT,
  PRICE_SORT,
  SEAT_SORT,
} from '../constants/otherDataConstant';

const initialState = {
  playlists: null,
  currentSort: null,
  currentPlaylist: null,
  availableSort: SORT_TYPE,
  playlistTracks:null
};

/**
 * Sorting Data Function
 */

// const sortArray = (type: any, state: any) => {
//   switch (type) {
//     case ETA_SORT.value: {
//       const duplicateETAState = [...state];
//       duplicateETAState.sort((a: any, b: any) => {
//         if (a.eta > b.eta) {
//           return 1;
//         } else if (a.eta == b.eta) {
//           return 0;
//         } else {
//           return -1;
//         }
//       });
//       return duplicateETAState;
//     }
//     case PRICE_SORT.value: {
//       const duplicatePriceState = [...state];
//       duplicatePriceState.sort((a: any, b: any) => {
//         if (a.price.amount > b.price.amount) {
//           return 1;
//         } else if (a.price.amount == b.price.amount) {
//           return 0;
//         } else {
//           return -1;
//         }
//       });
//       return duplicatePriceState;
//     }
//     case SEAT_SORT.value: {
//       const duplicateSeatState = [...state];
//       duplicateSeatState.sort((a: any, b: any) => {
//         if (a.product.maxSeats > b.product.maxSeats) {
//           return 1;
//         } else if (a.product.maxSeats == b.product.maxSeats) {
//           return 0;
//         } else {
//           return -1;
//         }
//       });
//       return duplicateSeatState;
//     }
//     default:
//       return [] as any;
//   }
// };

export default (state = initialState, action: any) => {
  switch (action.type) {
    /**
     * Set initital data
     */
    case ACTION_TYPES.LISITNG.SET_LISTING:
      return {
        ...state,
        playlists: action.payload,
      };

    case ACTION_TYPES.LISITNG.CLEAR_PLAYLISTS:
      return {
        ...state,
        playlists: null,
      };
    case ACTION_TYPES.LISITNG.SET_CURRENT_PLAYLIST:
      return {
        ...state,
        currentPlaylist: action.payload,
      };
    case ACTION_TYPES.LISITNG.CLEAR_CURRENT_PLAYLIST:
      return {
        ...state,
        currentPlaylist: null,
      };
      case ACTION_TYPES.LISITNG.SET_PLAYLIST_TRACKS:
        return {
          ...state,
          playlistTracks: action.payload,
        };

        case ACTION_TYPES.LISITNG.RESET_PLAYLIST_TRACKS:
      return {
        ...state,
        playlistTracks: null,
      };
    default:
      return state;
  }
};

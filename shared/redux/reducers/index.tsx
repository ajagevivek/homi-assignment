/**
 * This file includes all the reducers under reducers directory,
 * Import all and add to combineReducers to use any among whole app
 */
import { combineReducers } from 'redux';

import utils from './utils';
import user from './user';
import listing from './listing';


const rootReducer = combineReducers({
  utils,
  user,
  listing
});


export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>

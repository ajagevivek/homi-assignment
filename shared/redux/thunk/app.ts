/**
 * This is  place for application business logic
 */
import { splashLaunched } from '../actions/app';
import { AppDispatch } from '../store';



export const splashScreenLaunched = () => async (dispatch: AppDispatch) => {
  dispatch(splashLaunched());
};


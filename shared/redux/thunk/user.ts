/**
 * This is  place for application business logic
 */
import { login, logout } from '../actions/user';
import { AppDispatch } from '../store';
import { fetchAuthToken } from '../../api';
import { AxiosResponse } from 'axios';
import { setAuthHeader, resetAuthHeader } from '../../api/configureAPI';
import { Alert } from 'react-native';


export const loginApi = (data: { user: string, password: string }) => async (dispatch: AppDispatch) => {
  dispatch(login(data));
};

export const logoutApi = () => async (dispatch: AppDispatch) => {

  dispatch(logout());
  resetAuthHeader();
};

export const getAuthToken = (code: string, refreshToken: string) => async (dispatch: AppDispatch) => {
  console.log('code')
  fetchAuthToken(code, refreshToken).then((response: AxiosResponse) => {
    if (response.status == 200) {
      if (response.data.access_token) {
        dispatch(login(response.data));
        setAuthHeader(response.data.access_token)
      } else {
        Alert.alert('Oops!', 'Spotify Login Failed');
      }

    }
  }).catch((e) => {
    console.log(e.response)
  })
};

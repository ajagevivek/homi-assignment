/**
 * This is  place for application business logic
 */
import { setAvailibility, clearAvailibility,sortAvailibility,setCurrentPlaylist,resetCurrentPlaylist,setPlaylistTracks,resetPlaylistTracks } from '../actions/listing';
import { AppDispatch } from '../store';
import { fetchPlayListing,fetchPlaylistTracks } from '../../api';
import { AxiosResponse } from 'axios';

export const fetchListing = () => async (dispatch: AppDispatch) => {
  fetchPlayListing().then((response: AxiosResponse) => {
    console.log(response)
    if (response.status == 200) {
      dispatch(setAvailibility(response.data.playlists.items))
    }
  }).catch((e) => {
    console.log(e.response)
  })
};


export const getPlaylistTracks = (playlistId:string) => async (dispatch: AppDispatch) => {
  fetchPlaylistTracks(playlistId).then((response: AxiosResponse) => {
    console.log(response)
    if (response.status == 200) {
      dispatch(setPlaylistTracks(response.data.items))
    }
  }).catch((e) => {
    console.log(e.response)
  })
};

export const clearPlaylistTracks = () => async (dispatch: AppDispatch) => {
  dispatch(resetPlaylistTracks())
};



export const selectPlaylist = (data:any) => async (dispatch: AppDispatch) => {
  dispatch(setCurrentPlaylist(data))
};

export const deselectPlaylist = () => async (dispatch: AppDispatch) => {
  dispatch(resetCurrentPlaylist())
};

export const clearListing = () => async (dispatch: AppDispatch) => {
  dispatch(clearAvailibility());
};

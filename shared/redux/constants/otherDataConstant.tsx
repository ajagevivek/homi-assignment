/**
 * Sort Data Constant
 */
const ETA_SORT = {
  name: 'ETA',
  value: 'eta',
};

const PRICE_SORT = {
  name: 'Price',
  value: 'amount',
};

const SEAT_SORT = {
  name: 'Max Seats',
  value: 'maxSeats',
};

const SORT_TYPE = [ETA_SORT, PRICE_SORT, SEAT_SORT];

const DEFAULT_SORT = {
  name: 'ETA',
  value: 'eta',
};

export {SORT_TYPE, DEFAULT_SORT,SEAT_SORT,PRICE_SORT,ETA_SORT};

import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import { useSelector } from 'react-redux';
import { PostLoginBottomTab } from './postLogin';
import { PreLoginStack } from './preLogin';
import {RootState} from '../../shared/redux/reducers';
import {SCREENS } from '../constants/screen';


/**
 * Navigator
 */

const Drawer = (props: any) => {
  const userData: any = useSelector((state: RootState) => {
    return state.user;
  });

  return (
    <>
      {userData && userData.user ? (
        <PostLoginDrawer props={props} />
      ) : (
        <PreLoginDrawer props={props} />
      )}
    </>
  );
};

const PreLoginDrawer = (props: any) => {

  return (
    <>
      <PreLoginStack/>
    </>
  );
};

const PostLoginDrawer = (props: any) => {
  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator>
      <Drawer.Screen name={SCREENS.POST_LOGIN} component={PostLoginBottomTab} />
    </Drawer.Navigator>
  );
};

export default Drawer;

import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Login from '../view/screens/login';
import {TYPOGRAPHY} from '../view/styles/typography';
import {SCREENS } from '../constants/screen';

const PreLoginStack = (props: any) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={SCREENS.LOGIN}
        options={{
          headerShown:false,
        }}
        component={Login}
      />
    </Stack.Navigator>
  );
};

export {PreLoginStack};

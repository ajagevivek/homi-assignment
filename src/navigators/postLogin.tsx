import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Text, View} from 'react-native';
import 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from '../view/screens/home';
import RideDetails from '../view/screens/playlist-details';
import {SCREENS } from '../constants/screen';

import {TYPOGRAPHY} from '../view/styles/typography';


const HomeStack = (props: any) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={SCREENS.HOME}
        options={{
          headerShown:false,
        }}
        component={Home}
      />
      <Stack.Screen
        name={SCREENS.PLAYLIST_DETAILS}
        options={{
          headerTintColor:TYPOGRAPHY.COLOR.thirdColor,
          headerStyle:{backgroundColor:TYPOGRAPHY.COLOR.fourthColor},
          headerTitleStyle:{color:TYPOGRAPHY.COLOR.thirdColor}
          // headerShown:false,
        }}
        component={RideDetails}
      />
    </Stack.Navigator>
  );
};


const PostLoginBottomTab = () => {
  function SettingsScreen() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Settings!</Text>
      </View>
    );
  }

  const Tab = createMaterialBottomTabNavigator();

  return (
    <Tab.Navigator barStyle={{backgroundColor: TYPOGRAPHY.COLOR.fourthColor}}>
      <Tab.Screen
        options={{
          tabBarLabel: 'HomeStack',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="bus-marker" color={color} size={26} />
          ),
        }}
        name={SCREENS.HOME_STACK}
        component={HomeStack}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="dots-vertical-circle-outline"
              color={color}
              size={26}
            />
          ),
        }}
        name={SCREENS.SETTING}
        component={SettingsScreen}
      />
    </Tab.Navigator>
  );
};

export {PostLoginBottomTab};

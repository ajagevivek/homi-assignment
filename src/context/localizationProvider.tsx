import React from 'react';
import { withTranslation } from 'react-i18next';
import '../../i18n';
import LocalizationContext from "./localizationcontext";

/**
 * Localization Provider
 */

const LocalizationProvider = (props:any) => {
  
    const { t, i18n, children } = props;
     return (
      <LocalizationContext.Provider value={{ t, i18n }}>
        { children }
      </LocalizationContext.Provider>
    )
}

export default withTranslation()(LocalizationProvider);
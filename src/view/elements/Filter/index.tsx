import React, {useEffect, useState} from 'react';
import {Dimensions, ScrollView, TouchableOpacity, View} from 'react-native';
import CheckBox from 'react-native-check-box';
import {Menu, MenuOptions, MenuTrigger} from 'react-native-popup-menu';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {TYPOGRAPHY} from '../../styles/typography';
import {CText} from '../Text';
import styles from './styles';

const {width, height} = Dimensions.get('window');

type FilterType = {
  value: string;
  name: string;
};

export interface Props {
  availableSorts: FilterType[];
  currentSort:any;
  onApply:(data:any)=>void;
}

interface State {}

const Filter = (props: Props) => {
  const [menuStatus, setMenuStatus] = useState(false);

  const [selectedTag, setSelectedTag] = useState(null as any);

  useEffect(()=>{
    setSelectedTag(props.currentSort)
  },[])

  return (
    <Menu opened={menuStatus}>
      <MenuTrigger
        onPress={() => {
          setMenuStatus(true);
        }}>
        <MaterialCommunityIcons
          name="sort"
          color={TYPOGRAPHY.COLOR.thirdColor}
          size={26}
        />
      </MenuTrigger>
      <MenuOptions optionsContainerStyle={styles.optionContainer}>
        <View style={styles.filterHeaderBox}>
          <CText style={styles.filterText}>Filters</CText>
          <MaterialCommunityIcons
            onPress={() => setMenuStatus(false)}
            name="close"
            color={TYPOGRAPHY.COLOR.fourthColor}
            size={26}
          />
        </View>
        <ScrollView style={{maxHeight: height * 0.5}}>
          {props &&
            props.availableSorts &&
            props.availableSorts.map((data: FilterType, ind) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    setSelectedTag(data);
                  }}
                  key={'menu_' + ind}
                  style={styles.filterOption}>
                  <CheckBox
                    onClick={() => {
                      setSelectedTag(data);
                    }}
                    isChecked={selectedTag ?selectedTag.value == data.value:false}
                    checkBoxColor={TYPOGRAPHY.COLOR.Primary}
                    rightTextStyle={{
                      color: TYPOGRAPHY.COLOR.Primary,
                    }}
                    rightText={data.name}
                  />
                </TouchableOpacity>
              );
            })}
        </ScrollView>

        <View style={styles.applyContainer}>
          <TouchableOpacity
            onPress={() => {
              props.onApply(selectedTag)
              setMenuStatus(false);
            }}
            style={styles.appleBox}>
            <CText style={styles.applyText}>Apply</CText>
          </TouchableOpacity>
        </View>
      </MenuOptions>
    </Menu>
  );
};

export {Filter};

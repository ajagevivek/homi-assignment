import { StyleSheet } from 'react-native';

import { TYPOGRAPHY } from '../../styles/typography';

const styles = StyleSheet.create({
  optionContainer: {
    marginTop: 40,
    padding: 20,
    paddingVertical: 17.5,
    borderRadius: 10,
  },
  filterHeaderBox:{
    flexDirection: 'row',
    marginBottom: 10,
    width: '100%',
    justifyContent: 'space-between',
  },
  filterText:{
    color:TYPOGRAPHY.COLOR.fourthColor,
    fontSize: 18,
  },
  scrollViewBox:{},
  appleBox:{
    padding: 10,
    width:'100%',
    paddingHorizontal: 20,
    backgroundColor:TYPOGRAPHY.COLOR.Primary,
    borderRadius: 10,
  },
  applyContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  applyText:{
    color: TYPOGRAPHY.COLOR.thirdColor,
    textAlign:'center',
    fontSize: 14,
  },
  filterOption:{
    paddingVertical: 5
  }
});

export default styles;

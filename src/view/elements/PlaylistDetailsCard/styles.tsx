import {StyleSheet} from 'react-native';

import {TYPOGRAPHY} from '../../styles/typography';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
    borderRadius: 10,
    marginVertical: 5,
    width: '90%',
    backgroundColor: "#ffffff10",
  },

  numoftrack:{
    color:'#ffffff',
  },


  boxWrapper: {
    flexDirection: 'column',
    marginTop: 20,
    flex: 1,
  },
  supplierBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1,
    marginVertical: 10,
    zIndex: 99,
  },
  topRow: {
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-between',
  },
  bottomRow: {
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  supplierName: {
    color: TYPOGRAPHY.COLOR.fourthColor,
    fontSize: 20,
    textAlign: 'center',
  },
  priceText: {
    color: TYPOGRAPHY.COLOR.Primary,
    fontSize: 24,
    marginRight: 10,
    textAlign: 'center',
  },
  carSeat: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  etaBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  etaText: {
    color: TYPOGRAPHY.COLOR.fourthColor,
    fontSize: 12,
    textAlign: 'center',
  },
  seatText: {
    color: TYPOGRAPHY.COLOR.fourthColor,
    fontSize: 20,
    textAlign: 'center',
    marginLeft: 5,
  },
  logo: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    opacity: 0.4,
  },
  headerlogo: {},

  songName:{
    color:TYPOGRAPHY.COLOR.thirdColor,
    fontSize:16
  },
  artistName:{
    color:TYPOGRAPHY.COLOR.fifthColor,
    fontSize:10
  }
});

export default styles;

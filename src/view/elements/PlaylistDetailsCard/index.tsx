import * as React from 'react';
import { Image, Linking, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { CText } from '../Text';
import styles from './styles';




export interface Props {}

interface State {}

const PlaylistDetailsCard = (props: any) => {
  const song = props.data;

  const artistName = song.track.artists && song.track.artists[0]?song.track.artists[0].name : 'N/A';
  return (
    <TouchableOpacity onPress={()=>{
      props.onPress(song)
    }}  activeOpacity={0.9} style={styles.container}>
      <Image
        source={{uri: song.track.album.images[0].url}}
        style={{width: 80, height: 80, marginRight: 5}}
      />
      
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            display: 'flex',
            marginLeft: 5,
            width: '80%',
            alignItems: 'flex-start',
            height: '100%',
            flexDirection: 'column',
          }}>
          <CText style={styles.songName}>{song.track.name}</CText>
          <CText style={styles.artistName}>{artistName}</CText>
          <CText style={styles.numoftrack}>🔥{song.track.popularity}</CText>
        </View>
        <MaterialCommunityIcons onPress={()=>{
            Linking.openURL(song.track.uri)
        }} style={{marginRight:10}} color="#ffffff" name="play-circle" size={30} />
      </View>
    </TouchableOpacity>
  );
};

export { PlaylistDetailsCard };


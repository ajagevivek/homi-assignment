import { StyleSheet } from 'react-native';
import {View, TouchableOpacity, Image, Dimensions} from 'react-native';

import { TYPOGRAPHY } from '../../styles/typography';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    padding: width*0.015,
    // backgroundColor:'#000'
  },
  despText:{
    marginVertical:5,
    fontSize:12,
    color:'#fff'
  },
  numoftrack:{
    position: 'absolute',
    right:0,
    color:'#ffffff',
    backgroundColor:TYPOGRAPHY.COLOR.Primary,
    zIndex:99,
    paddingHorizontal:10,
    borderTopRightRadius:10,
    borderBottomLeftRadius:10
  }
  
});

export default styles;

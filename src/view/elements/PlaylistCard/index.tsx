import * as React from 'react';
import { Dimensions, Image, TouchableOpacity, View } from 'react-native';
import { CText } from '../Text';
import styles from './styles';


const {width, height} = Dimensions.get('window');

export interface Props {}

interface State {}

const PlaylistCard = (props: any) => {
  const playlist = props.data;

  const playlistImage = playlist.images[0].url;
  return (
    <TouchableOpacity
      onPress={() => props.onPress(props.data)}
      activeOpacity={0.9}
      style={styles.container}>
      <View style={{width: width * 0.47, borderRadius: 10}}>
        <CText style={styles.numoftrack}>{playlist.tracks.total}</CText>
        <Image
          style={{width: width * 0.47, height: width * 0.45, borderRadius: 10}}
          source={{uri: playlistImage}}
        />
        <CText style={styles.despText}>{playlist.description}</CText>
      </View>
    </TouchableOpacity>
  );
};

export { PlaylistCard };


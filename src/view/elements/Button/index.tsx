import * as React from 'react';
import { TouchableOpacity, ViewStyle,TextStyle } from 'react-native';

import { GLOBAL } from '../../styles/global';
import { CText } from '../Text';

type Callback = () => any;
export interface Props {
  title: string;
  onClick: Callback;
  style?: ViewStyle;
  textStyle?:TextStyle;
  activeOpacity?:any
}

/**
 * Default Button
 */
const BUTTON_DEFAULT = ({ title, onClick,textStyle, style,activeOpacity }: Props) => (
  <TouchableOpacity
    activeOpacity={activeOpacity||0.5}
    style={[style]}
    onPress={() => onClick()}
  >
    <CText style={textStyle}>{title}</CText>
  </TouchableOpacity>
);

export { BUTTON_DEFAULT };

import {StyleSheet} from 'react-native';

import {TYPOGRAPHY} from '../../styles/typography';

const styles = StyleSheet.create({
  modalStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#00000060',
  },
  closeIcon: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  webviewView:{
    width: '90%',
    height: '90%',
    borderRadius: 20,
    overflow: 'hidden',
  }
});

export default styles;

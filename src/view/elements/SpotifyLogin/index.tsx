import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  TouchableOpacity,
  ViewStyle,
  TextStyle,
  View,
  Modal,
} from 'react-native';
import {WebView} from 'react-native-webview';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import qs from 'qs';
import {GLOBAL} from '../../styles/global';
import {CText} from '../Text';
import CONSTANTS from '../../../../config/index';
import styles from './styles';
type Callback = () => any;

export interface Props {
  onSuccess: Function;
  show: boolean;
}

/**
 * Default Button
 */
const SpotifyLogin = ({onSuccess, show}: Props) => {
  const [showModal, setModal] = useState(false);

  const _onNavigationStateChange = async (webViewState: any) => {
    const {url} = webViewState;
    console.log(url);
    if (url && url.startsWith(CONSTANTS.SPOTIFY_REDIRECT_DOMAIN)) {
      const match = url.match(/(#|\?)(.*)/);
      if (match && match[2]) {
        const results = qs.parse(match[2]);
        if (results.code) {
          console.log(results.code);
          setModal(false);
          onSuccess(results.code);
        }
      }
    }
  };

  useEffect(() => setModal(show), [show]);

  return (
    <Modal animationType="slide" visible={showModal} transparent>
      <View style={styles.modalStyle}>
        <MaterialCommunityIcons
          style={styles.closeIcon}
          color={'#ffffff'}
          name="close"
          size={25}
        />
        <View style={styles.webviewView}>
          <WebView
            onNavigationStateChange={(webViewState) => {
              _onNavigationStateChange(webViewState);
            }}
            source={{
              uri:
                'https://accounts.spotify.com/authorize?client_id=361d1d6c1ad447a7a58fe6fdc189463b&response_type=code&redirect_uri=https%3A%2F%2Fexample.com%2Fcallback&scope=user-read-private%20user-read-email%20playlist-read-private%20user-read-email',
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export {SpotifyLogin};

import { StyleSheet } from 'react-native';

import { TYPOGRAPHY } from '../../styles/typography';
import { GLOBAL } from '../../styles/global';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent:'flex-end',
    alignItems: 'center',
    backgroundColor: TYPOGRAPHY.COLOR.fourthColor,
  },
  loginBox:{
    width:'90%',
    alignItems: 'center',
    paddingTop:20,
    backgroundColor:TYPOGRAPHY.COLOR.thirdColor,
    marginBottom:'10%',
    borderRadius:10
  },
  logo:{
    marginBottom:100,
  },
  image: {
    marginVertical:10,
  },
  welcomeText:{
    ...GLOBAL.FONTS.headingText,
    textAlign:'center',
    marginVertical:10
  }
  
});

export default styles;

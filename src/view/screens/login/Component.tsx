import React, {useEffect, useState} from 'react';
import {Image, ImageBackground, View} from 'react-native';
import LocalizationContext from '../../../context/localizationcontext';
import {BUTTON_DEFAULT} from '../../elements/Button';
import {SpotifyLogin} from '../../elements/SpotifyLogin';
import {CText} from '../../elements/Text';
import {GLOBAL} from '../../styles/global';
import styles from './styles';

export interface Props {
  loginApi: (user: {user: string; password: string}) => void;
  logoutApi: Function;
  getAuthToken: Function;
  navigation: any;
  route: any;
}

const Login = (props: Props) => {
  const {t, i18n} = React.useContext(LocalizationContext);
  const [showLogin, setShowLogin] = useState(false);

  useEffect(() => {}, []);

  const getSpotifyToken = (code: string) => {
    props.getAuthToken(code);
  };

  return (
    <>
      <SpotifyLogin
        show={showLogin}
        onSuccess={(code: string) => {
          getSpotifyToken(code);
        }}
      />
      <ImageBackground
        resizeMode="cover"
        imageStyle={{opacity: 0.4}}
        source={{uri: 'https://media.giphy.com/media/svw5mZJdFB41G/giphy.gif'}}
        style={styles.container}>
        <Image
          resizeMode="contain"
          source={{
            uri:
              'https://storage.googleapis.com/pr-newsroom-wp/1/2018/11/Spotify_Logo_CMYK_White.png',
          }}
          style={{
            width: '50%',
            height: 400,
          }}
        />
        <View style={styles.loginBox}>
          <CText style={styles.welcomeText}>{`${t('welcome', i18n)}`}</CText>
          <BUTTON_DEFAULT
            style={GLOBAL.CTA.Style.secondarySecond}
            textStyle={GLOBAL.CTA.CtaTextStyle.secondary}
            title={`${t('change_language', i18n)}`}
            onClick={() => {
              if (i18n.language == 'fr') {
                i18n.changeLanguage('en');
              } else {
                i18n.changeLanguage('fr');
              }
            }}
          />
          <BUTTON_DEFAULT
            style={GLOBAL.CTA.Style.primary}
            textStyle={GLOBAL.CTA.CtaTextStyle.primary}
            title={`${t('login_button', i18n)}`}
            onClick={() => {
              setShowLogin(true);
              //props.loginApi({user: 'Vivek', password: 'Password'});
            }}
          />
        </View>
      </ImageBackground>
    </>
  );
};
export default Login;

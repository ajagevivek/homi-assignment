import { connect } from 'react-redux';

import Component from './Component';
import {
  loginApi,
  logoutApi,
  getAuthToken
} from '../../../../shared/redux/thunk/user';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  loginApi: (data:{user:string,password:string}) => dispatch(loginApi(data)),
  logoutApi:() => dispatch(logoutApi()),
  getAuthToken:(code:string,refreshToken:string) => dispatch(getAuthToken(code,refreshToken))
});

const Login = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default Login;

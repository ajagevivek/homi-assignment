import { StyleSheet } from 'react-native';

import { TYPOGRAPHY } from '../../styles/typography';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent:'flex-end',
    backgroundColor: TYPOGRAPHY.COLOR.Secondary,
  },
  image: {
    width: '70%',
  },
  topView:{
    flexDirection:'row',
    alignItems:'center',
    width:'90%',
    marginVertical:10,
    marginTop:20,
    justifyContent:'space-between'
  },
  activityIndicator:{
    marginTop:50
  },
  listing:{
    backgroundColor:TYPOGRAPHY.COLOR.fourthColor,
    width: '100%',
    alignItems:'center',
    justifyContent:'center',
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
    height:'82.5%'
  },
  scrollViewContainerStyle:{
    alignItems:'center'
  },
  scrollViewStyle:{
    width:'100%',
  },
  listingHeader:{
    color:TYPOGRAPHY.COLOR.thirdColor,
    textAlign:'center',
    fontSize:20
  },

  headerText:{
    color:TYPOGRAPHY.COLOR.thirdColor,
    fontSize:30,
    textAlign:'left',
    width: '100%',
   
    marginLeft:30,
    marginVertical:10,
    marginTop:20,
  }
});

export default styles;

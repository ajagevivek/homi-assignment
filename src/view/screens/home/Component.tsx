import React, {useEffect} from 'react';
import {ImageBackground, SafeAreaView, ScrollView, View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector} from 'react-redux';
import {RootState} from '../../../../shared/redux/reducers';
import LocalizationContext from '../../../context/localizationcontext';
import styles from './styles';
import {PlaylistCard} from '../../elements/PlaylistCard';
import {CText} from '../../elements/Text';
import {SCREENS} from '../../../constants/screen';

type FilterType = {
  value: string;
  name: string;
};

export interface Props {
  fetchListing: () => void;
  selectPlaylist: (data: any) => void;
  deselectPlaylist: Function;
  logoutApi: Function;
  clearPlaylistTracks:Function;
  navigation: any;
  route: any;
}

const Home = (props: Props) => {
  const {t, i18n} = React.useContext(LocalizationContext);

  useEffect(() => {
    props.fetchListing();
    props.clearPlaylistTracks();
  }, []);

  const listings: any = useSelector((state: RootState) => {
    return state.listing;
  });

  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground
        imageStyle={{opacity: 1}}
        resizeMode="cover"
        source={{uri:'https://media.giphy.com/media/navajMPLHKpLq/giphy.gif'}}
        style={styles.container}>
        <View style={styles.listing}>
          <MaterialCommunityIcons
            onPress={() => {
              props.logoutApi();
            }}
            style={{position: 'absolute', top: 20, right: 10,zIndex:999}}
            color={'#ffffff'}
            name="account-cancel"
            size={30}
          />
          <CText style={styles.headerText} >
              {`${t('welcome_screen', i18n)}`}
            </CText>
          <ScrollView
            contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent:'space-between',
              width: '100%',
            }}
            style={{
              
            }}>
            {listings &&
              listings.playlists &&
              listings.playlists.length &&
              listings.playlists.map((playlist: any, index: number) => {
                return (
                  <PlaylistCard onPress={(data:any)=>{
                    props.selectPlaylist(data);
                    props.navigation.navigate(SCREENS.PLAYLIST_DETAILS);
                  }} key={'playlist_' + index} data={playlist} />
                );
              })}
          </ScrollView>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};
export default Home;

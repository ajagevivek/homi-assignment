import { connect } from 'react-redux';

import Component from './Component';
import {
  logoutApi
} from '../../../../shared/redux/thunk/user';
import {
  fetchListing,
  selectPlaylist,
  deselectPlaylist,
  clearPlaylistTracks
} from '../../../../shared/redux/thunk/listing';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchListing:()=> dispatch(fetchListing()),
  logoutApi:() => dispatch(logoutApi()),
  selectPlaylist:(data:any)=>dispatch(selectPlaylist(data)),
  deselectPlaylist:() => dispatch(deselectPlaylist()),
  clearPlaylistTracks:()=> dispatch(clearPlaylistTracks())
});

const home = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default home;

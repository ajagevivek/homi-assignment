import { connect } from 'react-redux';

import Component from './Component';
import {
  logoutApi
} from '../../../../shared/redux/thunk/user';
import {
  fetchListing,
  selectPlaylist,
  deselectPlaylist,
  getPlaylistTracks,
  clearPlaylistTracks
} from '../../../../shared/redux/thunk/listing';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchListing:()=> dispatch(fetchListing()),
  logoutApi:() => dispatch(logoutApi()),

  getPlaylistTracks:(playlistId:string)=>dispatch(getPlaylistTracks(playlistId)),
  clearPlaylistTracks:()=>dispatch(clearPlaylistTracks)
});

const playlistDetails = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default playlistDetails;

import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  Image,
  Linking,
  SafeAreaView,
  Modal,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import LocalizationContext from '../../../context/localizationcontext';
import {CText} from '../../elements/Text';
import {GLOBAL} from '../../styles/global';
import styles from './styles';
import {PlaylistDetailsCard} from '../../elements/PlaylistDetailsCard';
import {BUTTON_DEFAULT} from '../../elements/Button';
import {useSelector} from 'react-redux';
import {RootState} from '../../../../shared/redux/reducers';
import {TYPOGRAPHY} from '../../styles/typography';
import {Filter} from '../../elements/Filter';
import {fetchPlaylistTracks} from '../../../../shared/api';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

type FilterType = {
  value: string;
  name: string;
};

export interface Props {
  fetchListing: () => void;
  logoutApi: Function;
  getPlaylistTracks: Function;
  clearPlaylistTracks: Function;

  navigation: any;
  route: any;
}
const millisToMinutesAndSeconds = (millis: number) => {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ' Mins : ' + (seconds < 10 ? '0' : '') + seconds + ' Secs';
};

const PlaylistDetails = (props: Props) => {
  const {t, i18n} = React.useContext(LocalizationContext);
  const [currentSong, selectSong] = useState(null);

  const listing: any = useSelector((state: RootState) => {
    return state.listing;
  });

  useEffect(() => {
    const playlistId = listing.currentPlaylist.id;
    props.getPlaylistTracks(playlistId);
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      <Modal
        transparent
        animationType={'slide'}
        visible={currentSong ? true : false}>
        <View
          style={{
            backgroundColor: '#00000030',
            justifyContent: 'flex-end',
            height: '100%',
          }}>
          <View
            style={{
              backgroundColor: '#ffffff',
              alignItems: 'center',
              padding: 10,
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,
              width: '100%',
              height: '80%',
            }}>
            <MaterialCommunityIcons
              onPress={() => {
                selectSong(null);
              }}
              style={{position: 'absolute', top: 10, right: 10}}
              color="#000000"
              name="close"
              size={40}
            />
            {currentSong ? (
              <>
                <Image
                  style={{width: 200, height: 200, marginTop: 50}}
                  source={{uri: currentSong.track.album.images[0].url}}></Image>
                <CText
                  style={
                    styles.songDetailHeader
                  }>{`${currentSong.track.name}`}</CText>
                {currentSong.track.artists && currentSong.track.artists.map((artist: any) => {
                  
                  return (
                    <CText
                      style={styles.artistDetails}>{`${artist.name}`}</CText>
                  );
                })}
                <CText
                  style={
                    styles.albumName
                  }>{`${currentSong.track.album.name}`}</CText>
                <CText style={styles.duration}>{`${millisToMinutesAndSeconds(
                  currentSong.track.duration_ms,
                )}`}</CText>
                <MaterialCommunityIcons
                  onPress={() => {
                    Linking.openURL(currentSong.track.uri);
                  }}
                  style={{marginTop: 20}}
                  color="#000000"
                  name="play-circle"
                  size={70}
                />
              </>
            ) : null}
          </View>
        </View>
      </Modal>
      <View style={styles.container}>
        <View style={styles.listing}>
          <View style={styles.topView}>
            <CText
              style={
                styles.listingHeader
              }>{`${listing.currentPlaylist.name}`}</CText>
          </View>
          <ScrollView
            contentContainerStyle={styles.scrollViewContainerStyle}
            style={styles.scrollViewStyle}>
            {listing.playlistTracks ? (
              listing.playlistTracks.map((song: any, index: number) => {
                return (
                  <PlaylistDetailsCard
                    onPress={(currentSong: any) => {
                      selectSong(currentSong);
                    }}
                    key={'song_' + index}
                    t={t}
                    i18n={i18n}
                    data={song}
                  />
                );
              })
            ) : (
              <ActivityIndicator
                size="large"
                style={{marginTop: 100}}
                color={TYPOGRAPHY.COLOR.Primary}
              />
            )}
          </ScrollView>
          {/* <BUTTON_DEFAULT
            style={GLOBAL.CTA.Style.secondary}
            textStyle={GLOBAL.CTA.CtaTextStyle.primary}
            title={`${t('logout_button', i18n)}`}
            onClick={() => {
              props.logoutApi();
            }}
          /> */}
        </View>
      </View>
    </SafeAreaView>
  );
};
export default PlaylistDetails;

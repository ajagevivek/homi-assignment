import { StyleSheet } from 'react-native';

import { TYPOGRAPHY } from '../../styles/typography';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent:'flex-end',
    backgroundColor: TYPOGRAPHY.COLOR.Secondary,
  },
  image: {
    width: '70%',
  },
  topView:{
    flexDirection:'row',
    alignItems:'center',
    width:'90%',
    marginVertical:10,
    marginTop:20,
    justifyContent:'space-between'
  },
  activityIndicator:{
    marginTop:50
  },
  listing:{
    backgroundColor:'#121212',
    width: '100%',
    alignItems:'center',
    justifyContent:'center',
    height:'100%'
  },
  scrollViewContainerStyle:{
    alignItems:'center'
  },
  scrollViewStyle:{
    width:'100%',
  },
  listingHeader:{
    color:TYPOGRAPHY.COLOR.Primary,
    textAlign:'center',
    fontSize:40,
    marginBottom:20
  },
  songDetailHeader:{
    color:TYPOGRAPHY.COLOR.fourthColor,
    textAlign:'center',
    fontSize:30,
    marginBottom:10,
    marginTop:10
  },
  artistDetails:{
    color:TYPOGRAPHY.COLOR.Primary,
    textAlign:'center',
    fontSize:15,
  },
  duration:{
    color:TYPOGRAPHY.COLOR.fourthColor,
    textAlign:'center',
    fontSize:20,
    marginTop:5
  },
  albumName:{
    color:TYPOGRAPHY.COLOR.fourthColor,
    textAlign:'center',
    fontSize:20,
    marginTop:20
  }
});

export default styles;

import OTHER_ECO from './images/vehicle-other-eco.svg';
import STANDARD_SEDAN from './images/vehicle-standard-sedan.svg';
import STANDARD_SUV from './images/vehicle-standard-suv.svg';
import STANDARD_MINIBUS from './images/vehicle-standard-minibus.svg';
import OTHER_ACCESSIBLE from './images/vehicle-other-accessible.svg';

import BOLT from './images/supplier-bolt.svg';
import FREE_NOW from './images/supplier-freenow.svg';



const OTHER_ECO_STRING = OTHER_ECO.toString();
const STANDARD_SEDAN_STRING = STANDARD_SEDAN.toString();
const STANDARD_SUV_STRING = STANDARD_SUV.toString();
const STANDARD_MINIBUS_STRING = STANDARD_MINIBUS.toString();
const OTHER_ACCESSIBLE_STRING = OTHER_ACCESSIBLE.toString();


export const selectProviderLogo = (type: any): string => {
  let xmlstring = '';
  switch (
    `${type.supplierKey}`
  ) {
    case 'bolt':
      xmlstring = BOLT.toString();
      break;
    case 'freenow':
      xmlstring = FREE_NOW.toString();
      break;
    default:
      break;
  }

  return xmlstring;
};

export const selectCarImage = (type: any): string => {
    let xmlstring = '';
    switch (
      `${type.productType}_${type.vehicleType}`
    ) {
      case 'OTHER_ECO':
        xmlstring = OTHER_ECO_STRING;
        break;
      case 'STANDARD_SEDAN':
        xmlstring = STANDARD_SEDAN_STRING;
        break;
      case 'STANDARD_SUV':
        xmlstring = STANDARD_SUV_STRING;
        break;
      case 'STANDARD_MINIBUS':
        xmlstring = STANDARD_MINIBUS_STRING;
        break;
      case 'OTHER_ACCESSIBLE':
        xmlstring = OTHER_ACCESSIBLE_STRING;
        break;
      default:
        break;
    }

    return xmlstring;
  };




export {OTHER_ECO_STRING,STANDARD_SEDAN_STRING,STANDARD_SUV_STRING,STANDARD_MINIBUS_STRING,OTHER_ACCESSIBLE_STRING}

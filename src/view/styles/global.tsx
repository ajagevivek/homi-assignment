import {StyleSheet, Platform} from 'react-native';

import {TYPOGRAPHY} from './typography';

const CTA = {
  Style: StyleSheet.create({
    primary: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 48,
      paddingVertical: 8,
      paddingHorizontal: 20,
      borderRadius: 10,
      color: TYPOGRAPHY.COLOR.thirdColor,
      margin: 10,
      backgroundColor: TYPOGRAPHY.COLOR.Primary,
    },
    secondary: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 48,
      paddingVertical: 8,
      paddingHorizontal: 20,
      borderRadius: 10,
      color: TYPOGRAPHY.COLOR.thirdColor,
      margin: 10,
      borderWidth: 1,
      borderColor: TYPOGRAPHY.COLOR.thirdColor,
      backgroundColor: TYPOGRAPHY.COLOR.Primary,
    },
    secondarySecond: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 48,
      paddingVertical: 8,
      paddingHorizontal: 20,
      borderRadius: 10,
      color: TYPOGRAPHY.COLOR.Primary,
      margin: 10,
      borderWidth: 1,
      borderColor: TYPOGRAPHY.COLOR.Primary,
      backgroundColor: TYPOGRAPHY.COLOR.thirdColor,
    },
  }),
  CtaTextStyle: StyleSheet.create({
    primary: {
      fontFamily: TYPOGRAPHY.FONT.Primary,
      fontSize: 18,
      textAlign: 'center',
      width: '100%',
      color: TYPOGRAPHY.COLOR.thirdColor,
    },
    secondary: {
      fontFamily: TYPOGRAPHY.FONT.Primary,
      fontSize: 18,
      textAlign: 'center',
      width: '100%',
      color: TYPOGRAPHY.COLOR.Primary,
    },
  }),
};

const FONTS = StyleSheet.create({
  headingText: {
    fontSize: 18,
    fontFamily: TYPOGRAPHY.FONT.Primary,
    color: TYPOGRAPHY.COLOR.fourthColor,
  },
});

const TEXT = StyleSheet.create({
  Default: {
    textAlign: 'left',
    fontFamily: TYPOGRAPHY.FONT.Primary,
    fontSize: 14,
    color: TYPOGRAPHY.COLOR.Primary,
  },
  Bold: {
    textAlign: 'left',
    fontSize: 14,
    fontFamily: TYPOGRAPHY.FONT.Primary,
    color: TYPOGRAPHY.COLOR.Primary,
  },
});

const GLOBAL = {
  CTA,
  FONTS,
  TEXT,
};

export {GLOBAL};

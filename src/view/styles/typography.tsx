const COLOR = {
  // CTA
  Primary: '#1BD760',
  Secondary: '#E8F6E8',
  thirdColor:'#FFFFFF',
  fourthColor:'#000000',
  fifthColor:'#989898',

};

const FONT = {
  Primary: 'Helvetica',
};

const BUTTON = {
  radius: 5,
};

const TYPOGRAPHY = {
  COLOR,
  FONT,
  BUTTON,
};

export { TYPOGRAPHY };

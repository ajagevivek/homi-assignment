/**
 * Defined screen names as constants here
 */
const SCREENS = {
  POST_LOGIN:'postLogin',
  HOME:'Home',
  PLAYLIST_DETAILS:'Song List',
  HOME_STACK:'HomeStack',
  SETTING:'Settings',
  LOGIN:'Login'
};

export { SCREENS };

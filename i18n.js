import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
// the translations
// (tip: move them in separate JSON files and import them)
const resources = {
  en: {
    translation: {
      "welcome": `Welcome to Assignment Project \n(Current Language : English)`,
      "login_button":"LOGIN WITH SPOTIFY",
      'logout_button':'LOGOUT',
      'welcome_screen': 'Top Playlists',
      'change_language':'Change Language',
      'log':'Log into Native side'

    }
  },
  fr: {
    translation: {
      "welcome": `Bienvenue dans le projet d'affectation \n(langue actuelle: anglais)`,
      'login_button':"CONNEXION AVEC SPOTIFY",
      'logout_button':'SE DÉCONNECTER',
      'welcome_screen': 'Meilleures listes de lecture',
      'change_language':'Changer de langue',
      'log':'Connectez-vous côté natif'

    }
  },

};
i18n
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources,
    lng: "en",
    fallbackLng: 'en',
    debug: true,
    interpolation: {
      escapeValue: false
    }
  });
export default i18n;